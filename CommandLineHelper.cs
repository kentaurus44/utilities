﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class CommandLineHelper
{
    public static void RunCommand(string filename, params string[] args)
    {
        string arguments = "";
        for (int i = 0, count = args.Length; i < count; ++i)
        {
            arguments += args[i] + " ";
        }

        var p = new Process();
        p.StartInfo.FileName = filename;
        p.StartInfo.Arguments = arguments;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.CreateNoWindow = true;

        p.StartInfo.WorkingDirectory = "../Python/";
        p.StartInfo.UseShellExecute = false;
        p.Start();
        // Read the output - this will show is a single entry in the console - you could get  fancy and make it log for each line - but thats not why we're here
        //UnityEngine.Debug.Log("Imported Design with no errors");
        p.WaitForExit();
        p.Close();
    }
}
